class Person {
  String? firstName;
  String? lastName;
  String get fullName => "$firstName $lastName";
  String get initials => "${firstName![0]}.${lastName![0]}.";

  set fullName(String fullName) {
  var parts = fullName.split(" ");
  this.firstName = parts.first; this.lastName =
  parts.last;
}
}
main() {
  Person somePerson = new Person();
  somePerson.firstName = "Clark";
  somePerson.lastName = "Kent";
  print(somePerson.fullName); // prints Clark Kent
  print(somePerson.initials); // prints C. K.
  somePerson.fullName = "peter parker";
}