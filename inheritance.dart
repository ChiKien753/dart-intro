class Student extends Person {
  String nickName;

  Student(String firstName, String lastName, this.nickName,) : super(firstName, lastName, nickName);

  @override
  String toString() => "$nickName";
}

class Person {
  String firstName;
  String lastName;
  Person(this.firstName, this.lastName, String s);
  String getFullName() => "$firstName $lastName";
}
main() {
  var student = Student("Clark", "Kent", "Kal-El");
  print(student);
}