// class Person {
// late String firstName;
// late String lastName;
// Person(String firstName, String lastName) {
// this.firstName = firstName;
// this.lastName = lastName;
// }
// String getFullName() => "$firstName $lastName";
// }

class Person {
String firstName;
String lastName;
Person(this.firstName, this.lastName);
String getFullName() => "$firstName $lastName";
}
void main() {
// Person somePerson = Person(); No longer compiles
Person somePerson = Person("Clark", "Kent");
print(somePerson.getFullName());
}