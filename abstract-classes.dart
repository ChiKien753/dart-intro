abstract class Person {
String firstName;
String lastName;
Person(this.firstName, this.lastName);
String get fullName;
}

class Student extends Person {
  String nickName;

  Student(String firstName, String lastName, this.nickName,) : super(firstName, lastName);

  @override
  String toString() => "$nickName";

  @override
  // TODO: implement fullName
  String get fullName => throw UnimplementedError();
}
main() {
Person student = new Student("Clark", "Kent", "Kal-El");
// Works because we are instantiating the subtype
// Person p = new Person();
// abstract classes cannot be instantiated
print(student);
}