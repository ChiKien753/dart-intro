// main() {
// List placeNames = ["Middlesbrough", "New York"];
// placeNames.add(1);
// print("Place names: $placeNames");
// }
// prints Place names: [Middlesbrough, New York, 1]

main() {
List<String> placeNames = ["Middlesbrough", "New York"];
placeNames.add('s');
// add() expects a String so this doesn't compile
}