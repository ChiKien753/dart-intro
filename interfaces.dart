class Student implements Person {
String nickName;
@override
String firstName;
@override
String lastName;
Student(this.firstName, this.lastName, this.nickName);
@override
String get fullName => "$firstName $lastName";
@override
String toString() => "$fullName, also known as $nickName";
}
abstract class Person {
String firstName;
String lastName;
Person(this.firstName, this.lastName);
String get fullName;
}


main() {
Person student = new Student("Clark", "Kent", "Kal-El");
// Works because we are instantiating the subtype
// Person p = new Person();
// abstract classes cannot be instantiated
print(student);
}